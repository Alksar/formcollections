﻿using System;
using NUnit.Framework;
using Moq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace FormCollections.Tests
{
    [TestFixture]
    class StackFormsTests
    {
        [Test]
        public void Check_instance_stack_without_parameters()
        {
            //arrange
            StackForms emptyStack = new StackForms();

            //assert
            Assert.IsNotNull(emptyStack);
        }

        [Test]
        public void Instance_stack_with_null_IEnumerable_form_param_should_raise_ArgumentNullExeption()
        {
            //act+assert
            Assert.That(() => new StackForms(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Check_instance_stack_with_IEnumerable_form_param()
        {
            //arrange
            IEnumerable<Form> elements = new List<Form>()
            {
                new Form(),
                new Form(),
                new Form()
            };

            StackForms stack = new StackForms(elements);

            //act+assert
            for (int i = 0; i < stack.Count; i++)
            {
                Form currentForm = stack.Pop();
                Assert.AreEqual(elements.ElementAt(i), currentForm);
            }
        }

        [Test]
        public void Push_should_add_element_on_top_of_stack()
        {
            //arrange
            StackForms stack = new StackForms();

            //act
            Form pushed = new Form();
            stack.Push(pushed);

            //assert
            Form poped = stack.Pop();
            Assert.AreEqual(poped, pushed);
        }

        [Test]
        public void Peek_should_get_top_element_in_stack()
        {
            //arrange
            StackForms stack = new StackForms();
            Form topElement = new Form();
            stack.Push(topElement);

            //act
            Form peeked = stack.Peek();

            //assert
            Assert.AreEqual(topElement, peeked);
        }

        [Test]
        public void Peek_element_from_empty_stack_should_throw_InvalidOperationException()
        {
            //arrange
            StackForms stack =new StackForms();

            //act+assert
            Assert.That(() => stack.Peek(), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void Pop_element_from_empty_stack_should_throw_InvalidOperationException()
        {
            //arrange
            StackForms stack = new StackForms();

            //act+assert
            Assert.That(() => stack.Pop(), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void Clear_should_remove_all_elements_from_stack()
        {
            //arrange
            StackForms stack = new StackForms();
            Form form = new Form();
            stack.Push(form);

            //act
            stack.Clear();

            //assert
            Assert.AreEqual(stack.Count, 0);
        }

        [Test]
        public void CallMethodByName_call_with_SetDesctopLocation_x_y_params_should_edit_location_x_y_properties_from_top_stack()
        {
            //arrange
            StackForms stack = new StackForms();

            Form currenForm = new Form();
            int previousLocationX = 1, previousLocationY = 2;
            currenForm.SetDesktopLocation(previousLocationX, previousLocationY);

            stack.Push(currenForm);

            //act
            int nextLocationX = 3, nextLocationY = 4;
            stack.CallMethodByName("SetDesktopLocation", nextLocationX, nextLocationY);

            //assert
            Assert.AreEqual(currenForm.Location.X, nextLocationX);
            Assert.AreEqual(currenForm.Location.Y, nextLocationY);
        }

        [Test]
        public void CallMethodByName_call_with_ToString_params_should_return_form_description_from_top_stack()
        {
            //arrange
            StackForms stack = new StackForms();
            Form currenForm = new Form();

            stack.Push(currenForm);

            //act
            string formToString = (string)stack.CallMethodByName("ToString");

            //assert
            Assert.AreEqual(formToString, stack.Peek().ToString());
        }
    }
}
