﻿using System;
using NUnit.Framework;
using Moq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace FormCollections.Tests
{
    [TestFixture]
    public class LinkedListFormsTests
    {
        private LinkedListForms _collection;

        [OneTimeSetUp]
        public void Initialize()
        {
            var list = new List<Form>()
            {
                new Form(),
            };
            _collection = new LinkedListForms(list);
        }

        [Test]
        public void Check_instance_form_collection_without_parameters()
        {
            //arrange
            LinkedListForms emptyCollection = new LinkedListForms();

            //assert
            Assert.IsNotNull(emptyCollection);
        }

        [Test]
        public void Instance_form_collection_with_null_IEnumerable_form_param_should_raise_ArgumentNullExeption()
        {
            //act+assert
            Assert.That(() => new LinkedListForms(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Check_instance_form_collection_with_IEnumerable_form_param()
        {
            //arrange
            IEnumerable<Form> elements = new List<Form>()
            {
                new Form(),
                new Form(),
                new Form()
            };

            LinkedListForms collection = new LinkedListForms(elements);

            //act+assert
            for (int i=0; i<collection.Count; i++)
                Assert.AreEqual(elements.ElementAt(i), collection.ElementAt(i));
        }

        [Test]
        public void SetCurrentIndex_should_raise_ArgumentOutOfRangeException_for_negative_index()
        {
            //act+assert
            Assert.That(() => _collection.SetCurrentIndex(-1),
                Throws.TypeOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void SetCurrentIndex_should_raise_ArgumentOutOfRangeException_if_index_more_than_size()
        {
            //arrange
            int indexMoreThanSize = _collection.Count;

            //act+assert
            Assert.That(() => _collection.SetCurrentIndex(indexMoreThanSize),
                Throws.TypeOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void SetCurrentIndex_assign_the_value_which_returned_by_GetCurrentIndex()
        {
            //arrange
            int index = new Random().Next(0, _collection.Count - 1);

            //act
            _collection.SetCurrentIndex(index);

            //assert
            Assert.AreEqual(_collection.GetCurrentIndex(), index);;
        }

        [TestCase(new Int32[] { }, TestName = "Empty array")]
        [TestCase(new[] { 1 }, TestName = "One element array")]
        [TestCase(new[] { 1, 2, 3, 5, 7 }, TestName = "Sorted array")]
        [TestCase(new[] { 7, 5, 3, 2, 1 }, TestName = "Reversed sortarray")]
        [TestCase(new[] { 1, 24, 7, 33, 42, 73 }, TestName = "Random array")]
        public void SortByCountControlsOnForm_should_sort_collection(int[] controlCountArray)
        {
            //arrange
            List<Form> controlCountList = new List<Form>();
            foreach (var controlCount in controlCountArray)
            {
                controlCountList.Add(CreateFormWithCountControls(controlCount));
            }

            LinkedListForms sorted = new LinkedListForms(controlCountList);

            //act
            sorted.SortByCountControlsOnForm();

            //assert
            Assert.IsTrue(IsSortedByControlsCountOnFormCollection(sorted));
        }

        private Form CreateFormWithCountControls(int controlCount)
        {
            Form form = new Form();
            
            for (int i=0; i< controlCount; i++)
                form.Controls.Add(new Control());

            return form;
        }

        private bool IsSortedByControlsCountOnFormCollection(IEnumerable<Form> collection)
        {
            int collectionCount = collection.Count();

            if (collectionCount <= 1) return true;

            bool isSorted = true;

            var currentFormControlsCount = collection.First().Controls.Count;

            foreach (var form in collection)
            {
                int formControlsCount = form.Controls.Count;
                if (formControlsCount < currentFormControlsCount)
                {
                    isSorted = false;
                    break;
                }
                currentFormControlsCount = formControlsCount;
            }
            
            return isSorted;
        }

        [Test]
        public void CallMethodByName_call_with_SetDesctopLocation_x_y_params_should_edit_location_x_y_properties()
        {
            //arrange
            LinkedListForms collection = new LinkedListForms();

            Form currenForm = new Form();
            int previousLocationX = 1, previousLocationY = 2;
            currenForm.SetDesktopLocation(previousLocationX, previousLocationY);

            collection.AddFirst(currenForm);

            //act
            int nextLocationX = 3, nextLocationY = 4;
            collection.CallMethodByName("SetDesktopLocation", nextLocationX, nextLocationY);

            //assert
            Assert.AreEqual(currenForm.Location.X, nextLocationX);
            Assert.AreEqual(currenForm.Location.Y, nextLocationY);
        }

        [Test]
        public void CallMethodByName_call_with_ToString_params_should_return_form_description()
        {
            //arrange
            LinkedListForms collection = new LinkedListForms();
            Form currenForm = new Form();

            collection.AddFirst(currenForm);

            //act
            string formToString=(string)collection.CallMethodByName("ToString");

            //assert
            Assert.AreEqual(formToString, collection.First().ToString());
        }
    }
}
