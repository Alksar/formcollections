﻿using System;
using NUnit.Framework;
using Moq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FormCollections.Tests
{
    [TestFixture]
    public class FormXmlCacheManagerTests
    {
        [Test]
        public void Load_should_restor_forms_data_from_cache()
        {
            //arrage
            List<Form> forSave = new List<Form>();
            
            Form formWithButton = new Form();
            Button button = new Button();
            button.Text = "button";
            formWithButton.Controls.Add(button);
            forSave.Add(formWithButton);

            Form formWithLabel = new Form();
            Label label = new Label();
            label.Text = "lable";
            formWithLabel.Controls.Add(label);
            forSave.Add(formWithLabel);

            Form formWithTextBox = new Form();
            TextBox textBox = new TextBox();
            textBox.Text = "textBox";
            formWithTextBox.Controls.Add(textBox);
            forSave.Add(formWithTextBox);

            FormXmlCacheManager.Save(forSave);

            //act
            List<Form> forLoad = new List<Form>();
            FormXmlCacheManager.Load(forLoad);
                
            //assert
            Assert.AreEqual(forSave.Count, forLoad.Count);

            for(int i=0; i<forSave.Count; i++)
                Assert.IsTrue(IsIdenticalForms(forSave[i], forLoad[i]));
        }

        /// <summary>
        /// forms are identical if all controls of from are identical
        /// </summary>
        /// <param name="lh"></param>
        /// <param name="rh"></param>
        /// <returns></returns>
        private bool IsIdenticalForms(Form lh, Form rh)
        {
            if (lh.Controls.Count != rh.Controls.Count)
                return false;

            bool isIdentical = true;

            for (int i = 0; i < lh.Controls.Count; i++)
                if (!IsIdenticalControls(lh.Controls[i], rh.Controls[i]))
                {
                    isIdentical = false;
                    break;
                }

            return isIdentical;
        }

        /// <summary>
        /// controls are identical if all properties avaliable from serialization are equival
        /// </summary>
        /// <param name="lh"></param>
        /// <param name="rh"></param>
        /// <returns></returns>
        private bool IsIdenticalControls(Control lh, Control rh)
        {
            if (lh.GetType() != rh.GetType())
                return false;

            bool isIdentical = true;

            PropertyInfo[] fieldInfos = lh.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo fieldInfo in fieldInfos)
            {
                if (fieldInfo.CanRead && fieldInfo.CanWrite &&
                    fieldInfo.Name != "Font" && fieldInfo.Name != "Handle")
                {
                    object contentLh = fieldInfo.GetValue(lh, null);
                    object contentRh = fieldInfo.GetValue(rh, null);

                    if (contentLh != null && contentRh != null)
                    {
                        if (contentLh.GetType().IsSerializable)
                        {
                            if (contentLh.ToString() != contentRh.ToString())
                            {
                                isIdentical = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (contentLh != null || contentRh != null)
                        {
                            isIdentical = false;
                            break;
                        }
                    }
                }
            }
            return isIdentical;
        }
    }
}
