﻿using System;
using NUnit.Framework;
using Moq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace FormCollections.Tests
{
    [TestFixture]
    public class QueueFormsTests
    {
        [Test]
        public void Check_instance_queue_without_parameters()
        {
            //arrange
            QueueForms emptyQueue = new QueueForms();

            //assert
            Assert.IsNotNull(emptyQueue);
        }

        [Test]
        public void Instance_queue_with_null_IEnumerable_form_param_should_raise_ArgumentNullExeption()
        {
            //act+assert
            Assert.That(() => new QueueForms(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Check_instance_queue_with_IEnumerable_form_param()
        {
            //arrange
            IEnumerable<Form> elements = new List<Form>()
            {
                new Form(),
                new Form(),
                new Form()
            };

            QueueForms queue = new QueueForms(elements);

            //act+assert
            for (int i = queue.Count-1; i >=0; i--)
            {
                Form currentForm = queue.Dequeue();
                Assert.AreEqual(elements.ElementAt(i), currentForm);
            }
        }

        [Test]
        public void Peek_should_get_head_element()
        {
            //arrange
            QueueForms queue = new QueueForms();
            Form form = new Form();
            queue.Enqueue(form);

            //act
            Form peekResult = queue.Peek();

            //assert
            Assert.AreEqual(peekResult, queue.Dequeue());
        }

        [Test]
        public void Peek_from_empty_queue_should_throw_InvalidOperationException()
        {
            //arrange
            QueueForms queue = new QueueForms();

            //act+assert
            Assert.That(() => queue.Peek(), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void Enqueue_should_add_element_from_tail()
        {
            //arrange
            QueueForms queue = new QueueForms();

            //act
            Form form = new Form();
            queue.Enqueue(form);

            //assert
            Assert.AreEqual(form, queue.Dequeue());
        }

        [Test]
        public void Dequeue_should_return_and_remove_element_from_head()
        {
            //arrange
            QueueForms queue = new QueueForms();
            Form form = new Form();
            queue.Enqueue(form);

            Form head = queue.Peek();

            //act
            Form dequeueForm = queue.Dequeue();

            //assert
            Assert.AreEqual(head, dequeueForm);
        }

        [Test]
        public void Dequeue_from_empty_queue_should_throw_InvalidOperationException()
        {
            //arrange
            QueueForms queue = new QueueForms();

            //act+assert
            Assert.That(() => queue.Dequeue(), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void Clear_should_clear_all_queue_elements()
        {
            //arrange
            QueueForms queue = new QueueForms();
            queue.Enqueue(new Form());

            //act
            queue.Clear();

            //assert
            Assert.AreEqual(queue.Count, 0);
        }

        [Test]
        public void CallMethodByName_call_with_SetDesctopLocation_x_y_params_should_edit_location_x_y_properties_for_head()
        {
            //arrange
            QueueForms queue = new QueueForms();

            Form currenForm = new Form();
            int previousLocationX = 1, previousLocationY = 2;
            currenForm.SetDesktopLocation(previousLocationX, previousLocationY);

            queue.Enqueue(currenForm);

            //act
            int nextLocationX = 3, nextLocationY = 4;
            queue.CallMethodByName("SetDesktopLocation", nextLocationX, nextLocationY);

            //assert
            Assert.AreEqual(queue.Peek().Location.X, nextLocationX);
            Assert.AreEqual(queue.Peek().Location.Y, nextLocationY);
        }

        [Test]
        public void CallMethodByName_call_with_ToString_params_should_return_form_description_for_head()
        {
            //arrange
            QueueForms queue = new QueueForms();
            Form currenForm = new Form();

            queue.Enqueue(currenForm);

            //act
            string formToString = (string)queue.CallMethodByName("ToString");

            //assert
            Assert.AreEqual(formToString, queue.Peek().ToString());
        }
    }
}
