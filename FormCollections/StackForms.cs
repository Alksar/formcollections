﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormCollections
{
    public class StackForms
    {
        private LinkedListForms _list;

        public StackForms()
        {
            _list = new LinkedListForms();
        }

        public StackForms(IEnumerable<Form> collection)
        {
            _list = new LinkedListForms(collection);
        }
        
        public int Count => _list.Count;

        public void Clear()
        {
            _list.Clear(); ;
        }

        public Form Peek()
        {
            if (_list.Count == 0)
                throw new InvalidOperationException("Empty stack");
            return _list.First.Value;
        }

        public Form Pop()
        {
            if (_list.Count == 0)
                throw new InvalidOperationException("Empty stack");
            Form item = _list.First.Value;
            _list.RemoveFirst();
            return item;
        }

        public void Push(Form item)
        {
            _list.AddFirst(item);
        }

        public object CallMethodByName(string methodName, params object[] parameters)
        {
            return _list.CallMethodByName(methodName, parameters);
        }
    }
}
