﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormCollections
{
    public class LinkedListForms : LinkedList<Form>
    {
        private int _currentIndex = 0;

        public LinkedListForms(){}

        public LinkedListForms(IEnumerable<Form> collection) : base(collection){}

        public int GetCurrentIndex()
        {
            return _currentIndex;
        }

        public void SetCurrentIndex(int currentIndex)
        {
            if ((currentIndex < 0) || (currentIndex >= this.Count))
                throw new ArgumentOutOfRangeException(nameof(currentIndex));
            _currentIndex = currentIndex;
        }

        public void SortByCountControlsOnForm()
        {
            if (this.Count <= 1) return;
            
            Form[] array = new Form[this.Count];
            this.CopyTo(array, 0);
            Array.Sort(array, (a, b) => a.Controls.Count.CompareTo(b.Controls.Count));

            this.Clear();

            foreach (var form in array)
            {
                this.AddLast(form);
            }
        }

        public object CallMethodByName(string methodName, params object[] parameters)
        {
            if (Count == 0) throw new NullReferenceException("Empty collection");

            MethodInfo methodInfo = typeof(Form).GetMethod(methodName);

            object result = methodInfo.Invoke(this.ElementAt(_currentIndex), parameters);

            return result;
        }
    }
}
