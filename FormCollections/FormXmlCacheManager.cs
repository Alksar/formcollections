﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace FormCollections
{
    public static class FormXmlCacheManager
    {
        /// <summary>
        /// Save collection of form on project directory in xml format
        /// </summary>
        /// <param name="forms"></param>
        public static void Save(ICollection<Form> forms)
        {
            if (forms == null)
                throw new ArgumentNullException("forms");
            if (forms.Count == 0)
                return;
            
            var root = new XElement("Forms");

            foreach (var form in forms)
            {
                root.Add(FormToXml(form));
            }
            root.Save("Cache" + ".xml");
        }

        /// <summary>
        /// Load collection form from xml file (Cache.xml in project directory)
        /// </summary>
        /// <param name="forms"></param>
        public static void Load(ICollection<Form> forms)
        {
            if (forms == null) forms = new List<Form>();
            forms.Clear();

            string cache = @"Cache.xml";

            if (!File.Exists(cache)) 
                throw new Exception("cache does not exist");

            var root = XElement.Load(cache);
            var XMLForms = root.Elements();

            foreach (var XMLForm in XMLForms)
            {
                Control[] newControls = XMLForm.Elements()
                .Select(GetControl)
                .Select(c => c as Control)
                .ToArray();

                var form = new Form();
                form.Controls.AddRange(newControls);
                forms.Add(form);
            }
        }

        public static bool IsCacheExist()
        {
            return File.Exists("Cache.xml");
        }

        /// <summary>
        /// Get XElement discription from form
        /// </summary>
        /// <param name="form"></param>
        /// <returns>XElement form discription</returns>
        private static XElement FormToXml(Form form)
        {
            var root = new XElement("Form");

            foreach (XElement element in
                form.Controls
                    .OfType<Control>()
                    .Select(ControlToXml))
            {
                root.Add(element);
            }
            return root;
        }

        /// <summary>
        /// Get XElement description from control
        /// </summary>
        /// <param name="control"></param>
        /// <returns>XElement control discription</returns>
        private static XElement ControlToXml(Control control)
        {
            Type controlType = control.GetType();

            var root = new XElement("RootControl",
                // Control type
                new XAttribute("Type", controlType.AssemblyQualifiedName));

            PropertyInfo[] fieldInfos = controlType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo fieldInfo in fieldInfos)
            {
                if (fieldInfo.CanRead && fieldInfo.CanWrite &&
                    fieldInfo.Name != "Font" && fieldInfo.Name != "Handle")
                {
                    object content = fieldInfo.GetValue(control, null);

                    if (content != null && content.GetType().IsSerializable)
                    {
                        var serializer = new DataContractSerializer(content.GetType());
                        var str = new StringBuilder();
                        using (XmlWriter stream = XmlWriter.Create(str))
                        {
                            serializer.WriteObject(stream, content);
                        }
                        
                        XElement data = XElement.Parse(str.ToString());
                       
                        var element = new XElement("Property",
                            
                            new XAttribute("Name", fieldInfo.Name),
                            
                            new XAttribute("Type", fieldInfo.PropertyType.AssemblyQualifiedName), data);
                       
                        root.Add(element);
                    }
                }
            }
            return root;
        }

        /// <summary>
        /// Get control from XElement description
        /// </summary>
        /// <param name="xml"></param>
        /// <returns>(object)control</returns>
        private static object GetControl(XElement xml)
        {
            Type controlType = Type.GetType(xml.Attribute("Type").Value);

            object control = Activator.CreateInstance(controlType);

            IEnumerable<XElement> elements = xml.Elements("Property");
            foreach (XElement element in elements)
            {
                string name = element.Attribute("Name").Value;
          
                Type type = Type.GetType(element.Attribute("Type").Value);
                
                XNode first = element.Nodes().First();
                
                var serializer = new DataContractSerializer(type);

                object value;
                using (var stream = new MemoryStream(Encoding.Default.GetBytes(first.ToString())))
                {
                    value = serializer.ReadObject(stream);
                }
               
                if (value != null)
                {
                    PropertyInfo fieldInfo = controlType.GetProperty(name);
                    fieldInfo.SetValue(control, value, null);
                }
            }
            return control;
        }
    }
}
