﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormCollections
{
    public class QueueForms
    {
        private LinkedListForms _list;

        public QueueForms()
        {
            _list = new LinkedListForms();
        }

        public QueueForms(IEnumerable<Form> collection)
        {
            _list = new LinkedListForms(collection);

            //head is always current for CallMethodByName func
            _list.SetCurrentIndex(_list.Count-1);
        }

        public int Count => _list.Count;

        public void Clear()
        {
            _list.Clear(); ;
        }

        public Form Peek()
        {
            if (_list.Count == 0)
                throw new InvalidOperationException("Empty queue");
            return _list.Last.Value;
        }

        public void Enqueue(Form item)
        {
            _list.AddFirst(item);

            //head is always current for CallMethodByName func
            _list.SetCurrentIndex(_list.Count - 1);
        }

        public Form Dequeue()
        {
            if (_list.Count == 0)
                throw new InvalidOperationException("Empty queue");
            Form item = _list.Last.Value;
            _list.RemoveLast();

            //head is always current for CallMethodByName func
            if (_list.Count != 0)
                _list.SetCurrentIndex(_list.Count-1);

            return item;
        }

        public object CallMethodByName(string methodName, params object[] parameters)
        {
            return _list.CallMethodByName(methodName, parameters);
        }
    }
}
